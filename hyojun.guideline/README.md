# Hyojun.Guideline
`v0.1.1`

_See english version [here][English]_

Esta é a Guideline Padrão usada nos projetos do Hyojun. Tem todos os templates base e um gerador de página simples usando o grunt para protótipos e projetos estáticos.

Veja um exemplo da guideline e como os elementos e classes funcionam em [http://fbiz.bitbucket.org/hyojun.guideline/](http://fbiz.bitbucket.org/hyojun.guideline/)

[Veja a apresentação aqui][apresentacao].

Conteúdo da página:

[TOC]

[↩][top]

## Instalação

Você precisará ter o [nodejs][] — para grunt, bower / npm — e o [ruby][] — for sass — instalado no seu computador para usar o Hyojun.Guideline.

1. Instale o [grunt-cli][]:

        sudo npm install grunt-cli -g

2. Instale o [bower][]:

        sudo npm install bower -g

### Instalando e configurando no seu projeto

A instalação por enquanto é manual. Crie o `bower.json` e baixe os pacotes necessários:

1. Criando o `bower.json`:

        bower init

2. Baixe o Hyojun.guideline como uma dependência do bower:

        bower install https://bitbucket.org/fbiz/hyojun.guideline.git --save-dev

Existem 2 formas de usar a guideline: gerando as páginas estáticas com mustache ou migrando (por enquanto manualmente) para outro ambiente, como PHP, ASP.NET, etc:

### Mustache

Para páginas estáticas como protótipos ou documentações, é possível usar o grunt + mustache para gerar o site. Primeiro copie e baixe as dependências do nodejs para usar o grunt:

1. Copie o arquivo `package.json` do Hyojun.guideline:

        cp bower_components/Hyojun.Guideline/package.json .

2. Instale os pacotes:

        npm install

    _(Não é elegante, mas logo mais teremos um instalador)_

3. Copie os arquivos necessários do bower_components (será automatizado):

        cp -r bower_components/Hyojun.Guideline/{generator,Gruntfile.js,.jshintrc} .
        echo "{}" > generator/grunt/js.json
        git clone https://bitbucket.org/fbiz/hyojun.sass-standards.git
        mkdir assets
        mv hyojun.sass-standards/sass assets/sass
        rm -rf hyojun.sass-standards

Crie o arquivo `assets/sass/output/1306-desktop/guideline.scss` e aplique os imports:

    #!sass
    @import
        // guideline lib
        "Hyojun.Guideline/assets/sass/source/common/wrappers/lib",

        // Project's specific core files comes here, to
        // customize guideline colors and gridsystem page widths.

        // guideline elements (desktop, large screen, version)
        "Hyojun.Guideline/assets/sass/output/1306-desktop/guideline.scss"; // desktop version

Crie o arquivo `assets/sass/output/320-mobile/guideline.scss` e aplique os imports:

    #!sass
    @import
        // guideline lib
        "Hyojun.Guideline/assets/sass/source/common/wrappers/lib",

        // Project's specific core files comes here, to
        // customize guideline colors and gridsystem page widths.

        // guideline elements (mobile, smaller screen, version)
        "Hyojun.Guideline/assets/sass/output/320-mobile/guideline.scss"; // mobile version

No arquivo `generator/guideline/data/gl-base.json` aponte o arquivo `guideline.js` para o diretório do bower:

    ...
    "js": {
        "core": "/bower_components/Hyojun.Guideline/assets/js/dist/guideline.js"
    },
    ...

Rode a task `render` do grunt para gerar os arquivos HTML e SASS:

    grunt render

Rode um servidor local para ver a página rodando, por exemplo:

    python -m SimpleHTTPServer

Acesse o endereço local em http://localhost:8000/guideline/

### ASP.NET

Existe um template ASP.NET que está aplicado em [Hyojun.Bootstrap][]. Para usá-lo apenas baixe e instale direto da página do projeto.

### Contributing with other Languages

Leia este documento para entender como contribuir para este projeto (TODO).

[↩][top]

## Changelog

Histórico das versões acessível em [CHANGELOG.md][].

[top]: #markdown-header-template
[English]: https://bitbucket.org/fbiz/hyojun.guideline/src/HEAD/README-en.md

[ruby]: https://www.ruby-lang.org/en/
[bower]: http://bower.io/
[nodejs]: http://nodejs.org/download/
[grunt-cli]: http://gruntjs.com/getting-started/
[Hyojun.Bootstrap]: https://bitbucket.org/fbiz/tecnologia_modulo-padrao
[CHANGELOG.md]: https://bitbucket.org/fbiz/hyojun.guideline/src/HEAD/CHANGELOG.md
[apresentacao]: https://speakerdeck.com/mcarneiro/hyojun-guideline