// @grunt -task=comp-js -page=guideline

define([
	'mod/colors/color-utils'
], function(colorUtils){
	"use strict";

	var colorJs = net.brehaut.Color,
		ColorGroup,
		_calculateHSLDiff = function (item) {
			var baseColor;
			if (item.color) {
				return false;
			}
			baseColor = (this.get(item.hslDiff.base) || {}).color;
			if (baseColor) {
				item.color = colorUtils.decodeHSLDiff(
					[baseColor.getHue(), baseColor.getSaturation(), baseColor.getLightness()],
					[item.hslDiff.h, item.hslDiff.s / 100, item.hslDiff.l / 100]
				);
				item.hex = item.color.toCSS();
			}
			return true;
		},
		// Convert RGB to XYZ
		_rgbToXyz = function (r, g, b) {
			var X, Y, Z;

			r = r / 255;
			g = g / 255;
			b = b / 255;

			if (r > 0.04045) {
				r = Math.pow(((r + 0.055) / 1.055), 2.4);
			}
			else {
				r = r / 12.92;
			}

			if (g > 0.04045) {
				g = Math.pow(((g + 0.055) / 1.055), 2.4);
			}
			else {
				g = g / 12.92;
			}

			if (b > 0.04045) {
				b = Math.pow(((b + 0.055) / 1.055), 2.4);
			}
			else {
				b = b / 12.92;
			}

			r = r * 100;
			g = g * 100;
			b = b * 100;

			X = r * 0.4124 + g * 0.3576 + b * 0.1805;
			Y = r * 0.2126 + g * 0.7152 + b * 0.0722;
			Z = r * 0.0193 + g * 0.1192 + b * 0.9505;

			return [X, Y, Z];
		},
		// Convert XYZ to LAB
		_xyzToLab = function (x, y, z) {
			x = x / 95.047;
			y = y / 100.000;
			z = z / 108.883;

			if (x > 0.008856) {
				 x = Math.pow(x, (1/3));
			} else {
				x = (7.787 * x) + (16 / 116);
			}

			if (y > 0.008856) {
				y = Math.pow(y, (1/3));
			} else {
				y = (7.787 * y) + (16 / 116);
			}

			if (z > 0.008856) {
				z = Math.pow(z, (1/3));
			} else {
				z = (7.787 * z) + (16 / 116);
			}

			return [
				(116 * y) - 16, // CIE_L
				500 * (x - y),  // CIE_a
				200 * (y - z)   // CIE_b
			];
		},
		// Finally, use cie1994 to get delta-e using LAB (formula from http://en.wikipedia.org/wiki/Color_difference#CIE94)
		_cie1994 = function (x, y, isTextiles) {
			var k2, k1, kl, c1, c2,
				sh, sc, sl, da, db,
				dc, dl, dh,
				kh = 1,
				kc = 1;

			if (isTextiles) {
				k2 = 0.014;
				k1 = 0.048;
				kl = 2;
			} else {
				k2 = 0.015;
				k1 = 0.045;
				kl = 1;
			}

			x = {l: x[0], a: x[1], b: x[2]};
			y = {l: y[0], a: y[1], b: y[2]};

			c1 = Math.sqrt(Math.pow(x.a, 2) + Math.pow(x.b, 2));
			c2 = Math.sqrt(Math.pow(y.a, 2) + Math.pow(y.b, 2));

			sl = 1;
			sc = 1 + k1 * c1;
			sh = 1 + k2 * c1;

			dl = x.l - y.l;
			dc = c1 - c2;
			da = x.a - y.a;
			db = x.b - y.b;

			dh = Math.sqrt(da * da + db * db - dc * dc);
			dh = isNaN(dh) ? 0 : dh;

			return Math.sqrt(
				Math.pow((dl / (kl * sl)), 2) +
				Math.pow((dc / (kc * sc)), 2) +
				Math.pow((dh / (kh * sh)), 2)
			);
		};

	ColorGroup = function () {
		this.data = [];
	};
	ColorGroup.prototype = {
		length: function () {
			return this.data.length;
		},
		get: function (p_label) {
			var val = null;
			$.each(this.data, function (i, item) {
				if (item.label === p_label) {
					val = item;
					return false;
				}
			});
			return val;
		},
		remove: function (p_label) {
			var val = null;
			$.each(this.data, function (i, item) {
				if (item.label === p_label) {
					val = i;
					return false;
				}
			});
			return val ? this.data.splice(val, 1).pop() : null;
		},
		add: function (p_label, p_hex, p_hsl) {
			var color = {
				label: p_label,
				hex: p_hex,
				hslDiff: p_hsl
			};
			color.color = p_hex ? colorJs(p_hex) : null;
			this.remove(p_label);
			this.data.push(color);
			return color;
		},
		filter: function (p_pattern, p_reverse) {
			var items = [],
				clone = new ColorGroup(),
				reverse = !!p_reverse;

			$.each(this.data, function (i, item) {
				var matched = !!item.label.match(new RegExp(p_pattern));
				if (matched !== reverse) {
					items.push(item);
				}
			});
			clone.data = items;
			return clone;
		},
		sort: function () {
			var clone = new ColorGroup();
			clone.data = this.data.sort(function(a,b){
				var aH = Math.pow(a.color.getHue(), 2), // priority to hue
					aS = Math.pow(a.color.getSaturation() * 100, 2) * 0.01, // less priority to saturation
					aL = Math.pow(a.color.getLightness() * 100, 2) * 0.002, // lowest priority to lightness

					bH = Math.pow(b.color.getHue(), 2),
					bS = Math.pow(b.color.getSaturation() * 100, 2) * 0.01,
					bL = Math.pow(b.color.getLightness() * 100, 2) * 0.002;

				return aH + aS + aL < bH + bS + bL ? -1 : 1;
			});
			return clone;
		},
		sortByClosest: function (p_hex) {
			var clone = new ColorGroup(),
				from = colorJs(p_hex),
				oXyz = _rgbToXyz(from.getRed() * 255, from.getGreen() * 255, from.getBlue() * 255);
				oLab = _xyzToLab(oXyz[0], oXyz[1], oXyz[2]);

			clone.data = this.data.sort(function(a,b){
				var aXyz = _rgbToXyz(a.color.getRed() * 255, a.color.getGreen() * 255, a.color.getBlue() * 255),
					aLab = _xyzToLab(aXyz[0], aXyz[1], aXyz[2]),

					bXyz = _rgbToXyz(b.color.getRed() * 255, b.color.getGreen() * 255, b.color.getBlue() * 255),
					bLab = _xyzToLab(bXyz[0], bXyz[1], bXyz[2]),
					
					aDiff = _cie1994(aLab, oLab, false),
					bDiff = _cie1994(bLab, oLab, false);

				return aDiff < bDiff ? -1 : 1;
			});
			return clone;
		},
		calculateRelatives: function () {
			var totalColors = this.data.length,
				pendingColors = totalColors,
				safetyLoop = totalColors * 10,
				callback = function (i, item) {
					if (!_calculateHSLDiff.bind(this)(item)) {
						pendingColors--;
					}
				}.bind(this);

			while (pendingColors > 0) {
				pendingColors = totalColors;
				$.each(this.data, callback);
				
				// just to be sure that no infinite loop will occur
				safetyLoop--;
				if (safetyLoop < 0) {
					throw new Error("Infinite loop was going to happen when calculating the color relatives");
				}
			}
		}
	};

	return {
		construct: function () {
			return new ColorGroup();
		}
	};
});