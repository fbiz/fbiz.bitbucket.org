// @grunt -task=comp-js -page=guideline

// needs net.brehaut.Color
define([], function(){
	"use strict";

	var Permalink,
		defaultConfig = {
			linkClassName: 'gl-link',
			linkContent: '#',
			permalinkSelector: 'h2.gl, h2.gl-header, h3.gl, h4.gl, h5.gl, h6.gl'
		};

	Permalink = function (p_config) {
		this.config = $.extend({}, defaultConfig, p_config);
		this.holder = this.config.holder || $(document.body);
		this.linkClassName = this.config.linkClassName;
		this.linkContent = this.config.linkContent;
		this.titles = $(this.config.permalinkSelector, this.holder);
	};
	Permalink.prototype = {
		_applyPermalink: function () {
			this.titles.each(function (i, item) {
				if (item.id) {
					$(item).append('<a class="'+this.linkClassName+'" href="#'+item.id+'">'+this.linkContent+'</a>');
				}
			}.bind(this));
		},
		init: function () {
			this._applyPermalink();
			return this;
		}
	};

	return {
		construct: function (p_config) {
			return new Permalink(p_config);
		},
		plugin: function () {
			return new Permalink().init();
		}
	};
});

