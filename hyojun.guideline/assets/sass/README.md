#<a id="top"></a>SASS
    version: 0.1
    date: 28/10/2013

##Summary

* [Getting started](#getting-started)
* [Folder organization](#folder-organization)
* [Changelog](#changelog)

## <a id="getting-started"></a> Getting started

The home of all SASS files. There are some organizations and practices to be highly considered before starting working on these files.

[[back to top]](#top)

## <a id="folder-organization"></a> Folder organization

This folder contains the following structure:

* **~/sass/** - you're here;
    * **output/** - all files that will generate CSS to go to production; [More details](#README.md);
    * **source/** - all functions, mixins, variables, modules and components are here, they are only imported to the files inside `output` folder; [More details](#README.md);

[[back to top]](#top)

## <a id="changelog"></a> Changelog
### 0.1
    author: Marcelo Miranda Carneiro
    email: mcarneiro@fbiz.com.br
    date: 28/10/2013
    description: First input

[[back to top]](#top)