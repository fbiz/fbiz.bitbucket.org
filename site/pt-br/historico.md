# Histórico de publicação

* `v1.1.0` - 14/01/2016 por [Marcelo Carneiro](mcarneiro@fbiz.com.br)    
_Simplificação no texto e atualização de algumas ferramentas._    
* `v1.0.0` - 09/10/2014 por [Marcelo Carneiro](mcarneiro@fbiz.com.br)    
_Primeira publicação convertida para o Gitbook._