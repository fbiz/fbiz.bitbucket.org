# F.biz Handbook

Temos um documento vivo, que foi batizado como "[F.biz Handbook](http://goo.gl/KfDjTU)", com tudo relacionado ao trabalho.

É composto por informações que passam pela metodologia, contribuição para projetos internos, plataformas e canais de comunicação, reuniões, feedback constante, TechTalk, etc.

> Os documentos institucionais estão restritos apenas aos `@fbiz.com.br`.
