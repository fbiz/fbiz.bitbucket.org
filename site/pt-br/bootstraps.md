# Hyojun.bootstraps

Temos uma lista de bootstraps que nos ajudam a iniciar projetos de forma padronizada, já com uma série de utilidades que facilitam nossa vida.

## Hyojun.bootstrap (.net)

Nosso primeiro bootstrap e o start-up de todos os projetos em `ASP.NET (C# MVC3)` é feito a partir do Hyojun.bootstrap. O projeto é separado em:

* [Core](https://bitbucket.org/fbiz/hyojun.bootstrap-core/) - consiste nas classes usadas por back-end para um servidor web;
* [Template](https://bitbucket.org/fbiz/hyojun.bootstrap/) - formado pela camada de "assets" e "views". Também contém classes de back-end relacionados à banco de dados, inversores de controles, segurança, etc.

> O Hyojun.bootstrap já contém a implementação de outros projetos, como o Hyojun.guideline e a organização do Hyojun.sass-standards.

Para baixar, visite a [página do projeto no Bitbucket](https://bitbucket.org/fbiz/hyojun.bootstrap/) **_(necessário permissão para acesso)_**. Utilize a [página de issues](https://bitbucket.org/fbiz/hyojun.bootstrap/issues) para sugestões e bugs.

## Hyojun.html-bootstrap

O [Hyojun.html-bootstrap](https://bitbucket.org/fbiz/hyojun.html-bootstrap) é um bootstrap que segue a mesma estrutura do Hyojun.bootstrap, mas para a construção de sites estáticos utilizando o Jekyll.

## Hyojun.couch-bootstrap

Utilizado para sites com saída estática, mas que precisam de uma camada de administração que possa ser feita inteira por um programador de front-end.

O [Hyojun.couch-bootstrap](https://bitbucket.org/fbiz/hyojun.html-bootstrap) em conjunto com o TeamCity se torna uma ferramenta poderosa para a construção rápida de sites administráveis. 

## Hyojun.email-bootstrap

O [Hyojun.email-bootstrap](https://bitbucket.org/fbiz/hyojun.email-bootstrap) tem como objetivos principais padronizar, simplificar e automatizar a construção de e-mail marketings.

É um _pack_ de ferramentas que ajudam a organizar e faz com que a construção de e-mails seja algo muito mais rápida e preciso.

