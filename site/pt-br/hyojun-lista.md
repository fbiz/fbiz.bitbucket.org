# Lista dos projetos Hyojun

Quase todos os projetos convergem em nossos _bootstraps_, fazendo com que fiquem naturalmente associados. Abaixo a lista de projetos para quem se interesse em contribuir com o código fonte:

## Hyojun.nosrc

O [Hyojun.nosrc](https://bitbucket.org/fbiz/hyojun.nosrc/wiki/Home) é um gerador de imagens dummy para utilização em protótipos. Encontra-se em http://nosrc.fbiz.com.br.

## Hyojun.guideline

O [Hyojun.guideline](https://bitbucket.org/fbiz/hyojun.guideline) é a estrutura HTML com elementos úteis para a construção da guideline de um projeto.

Existe uma [apresentação](https://speakerdeck.com/mcarneiro/hyojun-guideline) que fala sobre os conceitos da construção modular com a guideline e é possível navegar pela [guideline de exemplo](/hyojun.guideline) que fala sobre seus elementos HTML.

## Hyojun.sass-standards

O [Hyojun.sass-standards](https://bitbucket.org/fbiz/hyojun.sass-standards/) é o conjunto de padrões de [organização](https://bitbucket.org/fbiz/hyojun.sass-standards/src) e [práticas de desenvolvimento no SASS](https://bitbucket.org/fbiz/hyojun.sass-standards/wiki/).

A apresentação explica os conceiros, com exeplos e estudo de caso. Está dividida em [parte 1](https://speakerdeck.com/mcarneiro/2) e [parte 2](https://speakerdeck.com/mcarneiro/2-1).

## Hyojun.proxy

O [Hyojun.proxy](https://bitbucket.org/fbiz/hyojun.proxy) é um sistema de proxy que normaliza o _mime-type_ dos arquivos em servidores que entregam apenas como `text/plain`.
