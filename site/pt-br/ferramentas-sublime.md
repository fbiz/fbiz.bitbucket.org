# Sublime Text

Todos os plugins abaixo são instaláveis através do [Package Control](https://sublime.wbond.net/installation#st2).

* [Sublime Linter](https://github.com/SublimeLinter/SublimeLinter-for-ST2) - _highlight dos erros direto no documento baseado no `.jshintrc` do projeto._
* [Hasher](https://github.com/dangelov/hasher) - _conversor de caracteres com "de" &#x27f7; "para" base64, entity, URI Component, Unicode e MD5._
* [Emmet](http://emmet.io/) - _ferramenta para edição de html e css._
* [Sublime ColorPicker](http://weslly.github.io/ColorPicker/) - _ferramenta para visualização, edição e inserção de cores em hexadecimal._