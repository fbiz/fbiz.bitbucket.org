# Serviços

Alguns serviços que utilizamos para resolver problemas do dia-a-dia.

## Fontes

* [Font Squirrel Generator](http://www.fontsquirrel.com/tools/webfont-generator) - _conversão de fontes nos formatos compatíveis com a web. (As fontes introduzidas devem ter licença de uso online e a ferramenta não converte nada da Adobe, por questões legais)._
* [IcoMoon](https://icomoon.io/app/) - _geração de fontes a partir de um conjunto de arquivos SVG._

## Comunicação

* [Appear.in](https://appear.in/) - _criação de vídeo-conferência com compartilhamento de tela que utiliza WebRTC, direto do browser, sem necessidade de registro ou plugin. Tem aplicativo para iphone e android._
* [Slack](https://fbiz.slack.com/) - _sistema de chat baseado em canais (uma espécie de mIRC) utilizado, como substituto ao e-mail, para divulgação de informações relevantes de um projeto._

## Mock-up e documentação

* [JSON Generator](http://www.json-generator.com/) - _gerador de dados aleatórios em JSON._
* [Gitbook](https://github.com/GitbookIO/gitbook) - _geração de documentações a partir de arquivos Markdown._

## Imagens

* [JpegMini](http://www.jpegmini.com/main/shrink_photo) - _compressão de arquivos JPG._
* [TinyPNG](https://tinypng.com/) - _compressão de arquivos PNG._
* [RIOT](http://luci.criosweb.ro/riot/) - _compressão de arquivos JPG, GIF e PNG, apenas para Windows._
* [Image Resizer](http://imageresizing.net/) - _serviço para resize, compressão e tratamento de imagens para .net via REST._

## Testes mobile

* [Ghostlab](http://vanamco.com/ghostlab/) - _teste de browser em diversos dispositivos ao mesmo tempo. Sincronização de reload, scroll position e interação com formulários._

## Variados

* [Shape Catcher](http://shapecatcher.com/) - _identifica um glyph a partir de um desenho. Útil para identificar o valor unicode._
* [Regular Expressions 101](https://regex101.com/) - _ferramenta muito boa para teste de regular expression_.
