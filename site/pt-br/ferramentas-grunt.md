# Grunt

Utilizamos o grunt para automatizar todos os processos do front-end, como pre-compilação, otimização, teste e validação.

Todos os projetos contém um `packages.json` e o [grunt-cli](https://github.com/gruntjs/grunt-cli) deve estar instalado no sistema para garantir a versão do grunt por projeto.

> **Importante**    
> Alguns módulos funcionam apenas como ponte entre um programa e o grunt. Nestes casos, é necessário tê-lo instalado na máquina.

* [jit-grunt](https://github.com/shootaroo/jit-grunt) `^0.8` - _pacote que faz o carregamento de módulos do grunt automaticamente e muito mais rápido._

    ```
    npm install jit-grunt@^0.8 --save-dev
    ```

* [time-grunt](https://github.com/sindresorhus/time-grunt) `^1.0` - _pacote que mostra quanto tempo cada task levou para executar._

    ```
    npm install time-grunt@^1.0 --save-dev
    ```

* [grunt-sass](https://github.com/sindresorhus/grunt-sass) `^1.1.0` - _módulo para execução do SASS._

    ```
    npm install grunt-sass@^1.1.0 --save-dev
    ```

* [grunt-contrib-requirejs](https://github.com/gruntjs/grunt-contrib-requirejs) `^0.4` - _módulo para execução do RequireJS optimizer._

    ```
    npm install grunt-contrib-requirejs@^0.4 --save-dev
    ```

* [grunt-contrib-jshint](https://github.com/gruntjs/grunt-contrib-jshint) `^0.10` - _validação dos arquivos JS com JSHint._

    ```
    npm install grunt-contrib-jshint@^0.10 --save-dev
    ```

* [grunt-scsslint](https://github.com/ahmednuaman/grunt-scss-lint) `^0.3` - _validação de arquivos SCSS com o SCSSHint._

    ```
    npm install grunt-scsslint@^0.3 --save-dev
    ```

* [grunt-exec](https://github.com/jharding/grunt-exec) `^0.4` - _execusão de comandos do shell._

    ```
    npm install grunt-exec@^0.4 --save-dev
    ```

* [grunt-contrib-watch](https://github.com/gruntjs/grunt-contrib-watch) `^0.6` - _executa tarefas quando os arquivos são alterados, adicionados ou excluídos._

    ```
    npm install grunt-contrib-watch@^0.6 --save-dev
    ```

* [grunt-contrib-imagemin](https://github.com/gruntjs/grunt-contrib-imagemin) `^0.8` - _compressão de imagens (jpg, gif e png)._

    ```
    npm install grunt-contrib-imagemin --save-dev
    ```

* [grunt-pngmin](https://github.com/zauni/pngmin) `^0.6` - _compressão de png utilizando o [png-quant](http://pngquant.org/)._

    ```
    npm install grunt-pngmin@^0.6 --save-dev
    ```

* [grunt-concurrent](https://github.com/sindresorhus/grunt-concurrent) `^2.1.0` - _executa tarefas paralelamente (ex.: connect + watch)._

    ```
    npm install grunt-concurrent@^2.1.0 --save-dev
    ```

* [grunt-contrib-connect](https://github.com/gruntjs/grunt-contrib-connect) `^0.9` - _cria um servidor de testes utilizando o nodejs._

    ```
    npm install grunt-contrib-connect@^0.9 --save-dev
    ```


* [grunt-contrib-clean](https://github.com/gruntjs/grunt-contrib-clean) `^0.6` - _apaga arquivos e diretórios._

    ```
    npm install grunt-contrib-clean@^0.6 --save-dev
    ```
