# Qualidade e identidade do código

Tentamos manter um padrão de desenvolvimento para garantir qualidade, reduzir o tempo de definição e fazer com que o código tenha "a cara da F.biz", independente do indivíduo que trabalhar no projeto.

> **Importante**    
> Todos os projetos são avaliados com base nestes documentos.    
> A leitura é obrigatória aos membros da equipe e parceiros externos.
> 
> Os documentos listados são utilizados na validação de códigos internos ou de parceiros.

## Front-end

* [Premissas de desenvolvimento front-end](http://goo.gl/kWdSEm) - _visão geral sobre o que é esperado nos projetos, suporte à browsers, etc._
* [Checklist para code-review](http://goo.gl/54gUps) - _lista utilizada na verificação dos códigos entregues à F.biz. Se aprofunda no HTML, SEO, SASS, JS, performance, etc._
* [Guia de construção de e-mails](http://goo.gl/9B5aQB) - _documento com descrições sobre técnicas seguras para o desenvolvimento de e-mails._

> Caso queira saber sobre as ferramentas que utilizamos, veja a seção [Ferramentas](ferramentas.md).

## Back-end

* [Premissas de desenvolvimento back-end](http://goo.gl/CP68L) - _padrões, versões de ambientes e banco de dados, convensão de nomes, segurança etc._
* [Boas Práticas no .NET](http://goo.gl/lKHDaG) - _boas práticas em assuntos como métodos e sobrecargas, propriedades, acesso à dados, etc._
* [Boas Práticas em MVC](http://goo.gl/bRt4Ux) - _boas práticas em assuntos como geração de URLs, OutputCache, segurança, ViewModels, Sessions, uso de JSON, envio de e-mails, etc._
* [Projeto de exemplo](https://bitbucket.org/fbiz/tecnologia_projeto-exemplo/src) **_(necessário permissão para acesso)_** - _repositório contendo alguns exemplos como Mapping, Migration, Model, etc._

## GIT

Todos os projetos hospedados na F.biz e criados a partir de 07/2013 estão em [http://www.bitbucket.org/fbiz/](http://www.bitbucket.org/fbiz/).

A F.biz utiliza o padrão de organização de branches conhecido como "Gitflow". A [apresentação sobre o tema](https://speakerdeck.com/mcarneiro/processo-de-trabalho-com-o-git) fala sobre a metodologia. Se preferir, [veja o vídeo da apresentação](http://goo.gl/6WBc8v), ela contém alguns exemplos práticos no _command line_.

## SVN

Projetos anteriores à 07/2013 estão hospedados no servidor de SVN da a F.biz em [https://svn.fbiz.com.br/svn/](https://svn.fbiz.com.br/svn/).

A idéia é migrar os repositórios para o Bitbucket, conforme surgir a necessidade. Quando for necessário trabalhar nestes ambientes, seguir as práticas definidas nos documentos abaixo:

* [SVN - Boas práticas](http://bit.ly/fbiz-svn-boas-praticas) - _regras de uso do branch, svn:ignore, mensagens de commit, etc._
* [SVN - Dicas e tutoriais](http://bit.ly/fbiz-svn-dicas-tutoriais) - _exemplos práticos de como usar o SVN._
