# Hyojun

Valorizamos os padrões de desenvolvimento e utilizamos um "conceito oriental" onde a idéia do mínimo esperado é "o melhor possível".

O artigo original que descreve esta idéia foi escrito pelo Jason Yip [Standards: excellence vs mediocrity](http://bit.ly/12dei4S) e [traduzido pelo Fabio Akita](http://www.akitaonrails.com/2013/05/17/traducao-padroes-excelencia-vs-mediocridade).

> _... the best should be the standard ..._

"Hyojun" é o nome da iniciativa open source de compartilhar o nosso jeito de trabalhar, idéias e ferramentas para contribuir com uma "web" melhor.

O Hyojun está sempre em desenvolvimento e é composto por qualquer iniciativa interna da F.biz. Existem desde projetos de documentação e definições até micro-frameworks.
