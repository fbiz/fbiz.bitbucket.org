# Plugins jquery e zepto

Plugins que já foram testados e homologados. Cada item indica se é compatível com ambos os frameworks e suas versões.

* [jquery-ui](https://github.com/fbiz/jquery-ui.git) `#~1.10` - _compatível com `jquery >1.9`, biblioteca de UI._
    
    ```
    bower install https://github.com/fbiz/jquery-ui.git#~1.10 --save-dev
    ```

* [jquery-mousewheel](https://github.com/fbiz/jquery-mousewheel.git) `#~3.1` - _compatível com `jquery ^1.9`, para normalização dos deltas para eventos do "mouse wheel"._

    ```
    bower install https://github.com/fbiz/jquery-mousewheel.git#~3.1 --save-dev
    ```

* [jquery-zclip](https://github.com/fbiz/jquery-zclip.git) `#~1.1` - _compatível com `jquery ^1.9`, para copiar texto para área de tranferencia com fallback para navegadores antigos._

    ```
    bower install https://github.com/fbiz/jquery-zclip.git#~1.1 --save-dev
    ```

* [jquery-elevatezoom](https://github.com/fbiz/elevatezoom.git) `#~2.2` - _compatível com `jquery ^1.9`, para efeito de zoom em imagem._

    ```
    bower install jquery-elevatezoom=https://github.com/fbiz/elevatezoom.git#~2.2 --save-dev
    ```

* [unveil](https://github.com/fbiz/unveil.git) `#~1.3` - _compatível com `jquery ^1.9` ou `zepto >1.1`, para "lazy loading" de imagem._

    ```
    bower install https://github.com/fbiz/unveil.git#~1.3 --save-dev
    ```

* [tinyscrollbar](https://github.com/fbiz/tinyscrollbar.git) `#~2.1` - _compatível com `jquery >1.9`, para scroll customizado, também possui versão sem `jquery`._

    ```
    bower install https://github.com/fbiz/tinyscrollbar.git#~2.1 --save-dev
    ```

# User interface e animação

* [gsap](https://github.com/fbiz/GreenSock-JS.git) `#~1.13` - _biblioteca leve, de alta performance para animações complexas ou dinâmicas que necessitam maior controle sobre o CSS Transition._

    ```
    bower install https://github.com/fbiz/GreenSock-JS.git#~1.13 --save-dev
    ```

* [skrollr](https://github.com/fbiz/skrollr.git) `#~0.6` - _biblioteca standalone para criação de efeito "parallax" compatível com mobile e AMD._

    ```
    bower install https://github.com/fbiz/skrollr.git#0.6 --save-dev
    ```

# Mobile

Ferramentas feitas exclusivamente para telas touch-screen ou focadas em sites responsivos.

* [enquire](https://github.com/fbiz/enquire.js.git) `#~2.1` - _css media query no javascript._

    ```
    bower install https://github.com/fbiz/enquire.js.git#~2.1 --save-dev
    ```

* [matchmedia](https://github.com/fbiz/matchMedia.js.git) `#~0.2` - _normalização do `matchMedia` para testar media queries via javascript._

    ```
    bower install https://github.com/fbiz/matchMedia.js.git#~0.2 --save-dev
    ```
* [fastclick](https://github.com/fbiz/fastclick.git) `#~1.0` - _remove o delay de 300ms que existe entre o click e o trigger do evento em projetos com telas touch._

    ```
    bower install https://github.com/fbiz/fastclick.git#~1.0 --save-dev
    ```

* [swipejs](https://github.com/fbiz/Swipe.git) `#~2.0` - _slider de 1 item para mobile leve e "touch friendly"._

    ```
    bower install https://github.com/fbiz/Swipe.git#~2.0
    ```