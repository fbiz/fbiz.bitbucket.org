# Javascript

Todos os plugins e bibliotecas listados nesta seção já foram utilizados muitas vezes em projetos da F.biz. Temos conhecimento aprofundado no funcionamento e limitações. 

Também estamos abertos e incentivamos a experimentação de outras ferramentas, desde que exista motivo relevante para o projeto. Entre em contato com o time da F.biz caso tenha alguma sugestão.

Conforme usamos novas ferramentas com sucesso, inserimos nesta lista.

> A ideia é manter um fork dessas bibliotecas [no github da F.biz](https://github.com/fbiz) para evitar problemas como: autores fazendo commits incompatíveis, mudança de nome, remoção de repositório, etc.

Todas as bibliotecas são instaladas via [bower](http://www.bower.io/).