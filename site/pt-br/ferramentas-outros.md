## Outras ferramentas

Algumas ferramentas ou serviços que não estão diretamente associados ao desenvolvimento de projetos.

## Fbiz.presentation

O [Fbiz.presentation](https://bitbucket.org/fbiz/fbiz.presentation) contém nosso tema oficial de apresentações para a F.biz aplicado em cima do [jekyller](https://github.com/shower/jekyller). A apresentação é gerada a partir de arquivos Markdown.

Todas as apresentações que são citadas neste site foram feitas utilizando esta ferramenta.