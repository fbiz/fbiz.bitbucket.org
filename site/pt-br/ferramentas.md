# Ferramentas

De geração de fontes aos plugins, esta é a lista das ferramentas mais utilizadas pela F.biz que se tornaram padrão de uso no dia-a-dia.

É recomendado o uso das ferramentas, serviços e plugins listados nesta seção. Caso tenha alguma sugestão de remoção, alteração ou ampliação da lista, entre em contato com o time da F.biz ou [abra uma issue aqui](https://bitbucket.org/fbiz/fbiz.bitbucket.org/issues/new).