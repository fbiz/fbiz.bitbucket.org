# Bibliotecas

Lista de frameworks, micro-frameworks e utilidades.

## Manipulação de DOM, eventos, etc.

* [jquery](https://github.com/fbiz/jquery.git) `#~2.1` - _suporte `IE9+`._

    ```
    bower install https://github.com/fbiz/jquery.git#~2.1 --save-dev
    ```

* [zepto](https://github.com/fbiz/zepto.git) `#~1.1` - _alternativa ao jQuery, mais leve e simples. Não suporta o `IE9-`._

    ```
    bower install https://github.com/fbiz/zepto.git#~1.1 --save-dev
    ```

## Pacotes de utilidades

* [mout](https://github.com/fbiz/mout.git) `#0.11` - _pacote de utilidades (array, object, string, math, etc.)._

    ```
    bower install https://github.com/fbiz/mout.git#~0.11 --save-dev
    ```

## Sistema de templates

* [mustache](https://github.com/fbiz/mustache.js.git) `#~2.1` - _javascript template engine._

    ```
    bower install https://github.com/fbiz/mustache.js.git#~2.1 --save-dev
    ```

* [handlebars](https://github.com/fbiz/handlebars.js.git) `#~4.0` - _extensão da [mustache](http://mustache.github.io/) com alguns helpers já incluso._

    ```
    bower install https://github.com/fbiz/handlebars.js.git#~4.0 --save-dev
    ```

## AMD

* [requirejs](https://github.com/fbiz/requirejs-bower.git) `#~2.1` - _gerenciador de modulos AMD._

    ```
    bower install https://github.com/fbiz/requirejs-bower.git#~2.1 --save-dev
    ```

* [almond](https://github.com/fbiz/almond.git) `#~0.3` - _versão mais leve da `requirejs`, sem lógica de carregamento ou definição dos módulos no "runtime", feita para saídas concatenadas._

    ```
    bower install https://github.com/fbiz/almond.git#~0.3 --save-dev
    ```
