# Parceiros

Os parceiros da F.biz são considerados extensão da equipe. Todas as premissas e padrões de desenvolvimentos devem ser seguidos da mesma forma que acontece dentro da agência. Veja a seção [Qualidade e identidade de código](qualidade-e-identidade.md).

Todas as entregas passam por um _code-review_ com base nestes documentos e recomendamos o uso de bibliotecas e plugins homologados na seção [Ferramentas](ferramentas.md).

Caso haja sugestão de alguma ferramenta, entre em contato com o time da F.biz para que possamos juntos estudar e discutir a melhor solução para o projeto.
