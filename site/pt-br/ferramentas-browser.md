# Browsers antigos

Bilbiotecas de polyfill para alguns recursos do HTML5.

> Devemos sempre colocar uma mensagem para usuários que acessam de browsers que não são 100% suportados utilizando o [http://www.browser-update.org/](http://www.browser-update.org/).

* [history.js](https://github.com/fbiz/history.js.git) `#~1.8` - _implementação das funcionalidades do HTML5 History/State APIs (pushState, replaceState, onPopState) crossbrowser._

    ```
    bower install https://github.com/fbiz/history.js.git#~1.8 --save-dev
    ```

* [html5shiv](https://github.com/fbiz/html5shiv.git) `#~3.7` - _possibilida o uso de HTML5 sectioning elements para o legado de Internet Explorer._

    ```
    bower install https://github.com/fbiz/html5shiv.git#~3.7 --save-dev
    ```

* [swfobject](https://github.com/fbiz/swfobject.git) `#2.2` - _script crossbrowser para inserção de swf._

    ```
    bower install https://github.com/fbiz/swfobject.git#2.2 --save-dev
    ```