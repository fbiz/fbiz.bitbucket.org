# SASS

Atualmente utilizamos o [libsass](http://sass-lang.com/libsass) via node-sass, que é executado através das [tasks do Grunt](ferramentas-grunt.md). 

Bibliotecas que utilizamos:

* [bourbon](https://github.com/fbiz/bourbon.git) `#~3.1` – _biblioteca de mixins para resolver problemas comuns dentro do sass._

    ```
    bower install https://github.com/fbiz/bourbon.git#~3.1 --save-dev
    ```

* [gs](https://github.com/fbiz/gs.git) `#~0.5` – _grid system semântico._

    ```
    bower install https://github.com/fbiz/gs.git#~0.5 --save-dev
    ```
