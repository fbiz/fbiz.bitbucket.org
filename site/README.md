# Tecnologia F.biz

Este site contém tudo que um funcionário ou parceiro precisa saber sobre a Tecnologia da F.biz.

É possível encontrar documentos relacionados à metodologia de trabalho, padrões de desenvolvimento, code-review, ferramentas, etc.

Este é um projeto vivo e estamos abertos à sugestões. Caso encontre algum erro ou não consiga entender algo, entre em contato com o time da F.biz ou [abra uma issue no repositório deste site](https://bitbucket.org/fbiz/fbiz.bitbucket.org/issues).

_[Última atualização em 14/01/2016](pt-br/historico.md)._