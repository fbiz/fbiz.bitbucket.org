<<<<<<< Updated upstream
# F.biz Bitbucket user website
`v1.0.0`

[Wiki](https://bitbucket.org/fbiz/fbiz.bitbucket.org/wiki/Home)
=======
# Hub da F.biz no Bitbucket

Projeto funciona como hub de informação da equipe de tecnologia da F.biz. O objetivo é que aponte para todos os documentos ativos que possam ser úteis para colaboradores e parceiros.

## Ambientes

Subir um commit em `master` significa publicar em produção. Não existe homologação, ou seja, os testes devem ser feitos localmente:

* **Produção** - [http://fbiz.bitbucket.org/](http://fbiz.bitbucket.org/)

## Como atualizar os projetos de exemplo

Este projeto contém apenas as páginas de exemplos ou documentações dos repositórios originais baixadas utilizando `git read-tree`, para facilitar a atualização.

Estas cópias tem um pequeno nível de alteração, como inserção do ribbon "fork on bitbucket", customização dos caminhos para funcionar no ambiente ou em casos de sites gerados, os arquivos finais (que normalmente não existem no repositório original). Portanto, apesar de raro, conflitos podem ocorrer ao fazer o subtree merge.

Abaixo a lista de projetos e como atualizá-los:

### Hyojun.git-immersion

Projeto é um subtree de http://bitbucket.org/fbiz/hyojun.git-immersion.git. Para atualizá-lo, siga os passos:

    git remote add hyojun.git-immersion git@bitbucket.org:fbiz/hyojun.git-immersion.git
    git fetch hyojun.git-immersion master
    git merge -s subtree -X subtree=hyojun.git-immersion/ hyojun.git-immersion/master --no-commit
    # confira o resultado
    git commit

### Hyojun.guideline

Projeto é um subtree de http://bitbucket.org/fbiz/hyojun.guideline.git. Para atualizá-lo, apenas digite:

    git remote add hyojun.guideline git@bitbucket.org:fbiz/hyojun.guideline.git
    git fetch hyojun.guideline master
    git merge -s subtree -X subtree=hyojun.guideline/ hyojun.guideline/master --no-commit
    # confira o resultado
    git commit

### Hyojun.hyojun.rich-media-ad-boilerplate

Projeto é um subtree de http://bitbucket.org/fbiz/hyojun.guideline.git. Para atualizá-lo, apenas digite:

    git remote add hyojun.rich-media-ad-boilerplate git@bitbucket.org:fbiz/hyojun.rich-media-ad-boilerplate.git
    git fetch hyojun.rich-media-ad-boilerplate master
    git merge -s subtree -X subtree=hyojun.rich-media-ad-boilerplate/ hyojun.rich-media-ad-boilerplate/master --no-commit
    # confira o resultado
    git commit

## Changelog

* [1.1.0](https://bitbucket.org/fbiz/fbiz.bitbucket.org/src/845d874b34734ce22bfc5d35bd6dd6344950a4ea/?at=1.1.0) - Adição do projeto Rich Media Ad Boilerplate
* [1.0.0](https://bitbucket.org/fbiz/fbiz.bitbucket.org/src/9e61a4443a791570f810533578d059db1a42c8a1/?at=1.0.0) - primeira versão 'HUB' com gitbook
* [0.1.1](https://bitbucket.org/fbiz/fbiz.bitbucket.org/src/8fd23427b4ab800257f2de8c3dcd13d8e53cf3f2/?at=0.1.1) - Atualização readme e changelog com merge do branch 0.1.1
* [0.1.0](https://bitbucket.org/fbiz/fbiz.bitbucket.org/src/b88d89458bf1476e89dea3c6dc430bcfc2dc4538/?at=0.1.0) - Primeiro release lançado para a equipe
>>>>>>> Stashed changes
