$(function() {

	// Detect basic events: panleft panright tap press
	$('.sample').hammer().bind('panleft panright tap press', function(e) {
		this.textContent = e.type.toUpperCase() +' gesture';
	});

	PreventGhostClick($('.sample'));

});