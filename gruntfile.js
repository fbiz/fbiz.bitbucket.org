
module.exports = function (grunt) {

	"use strict";

	require('jit-grunt')(grunt);

	if (grunt.option('verbose') === true) {
		require('time-grunt')(grunt);
	}

	grunt.initConfig({
		watch: {
			site: {
				files: [
					'site/**/*.md',
					'site/book.json'
				],
				tasks: ['default']
			}
		},
		copy: {
			gitbook: {
				files: [
					{
						expand: true,
						cwd: './out/',
						src: ['**'],
						dest: '.'
					}
				]
			}
		},
		exec: {
			gitbook: {
				cmd : function () {
					return "gitbook build site/ ./out";
				}
			}
		}
	});

	grunt.registerTask("default", ["exec:gitbook", "copy:gitbook"]);
};